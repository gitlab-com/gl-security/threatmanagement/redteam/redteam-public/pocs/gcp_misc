#!/bin/bash

#############################
# Run this tool to leverage GCP's "Asset Query" functionality to enumerate
# all publicly-accessible resources.
#
# https://cloud.google.com/sdk/gcloud/reference/asset/query
#
# This is a good script to feed into vulnerability scanners, web crawlers, etc.
# Enjoy!
#############################

if [ $# -eq 0 ]; then
  echo "Usage: $0 <GCP Org ID>."
  exit 1
fi

GCP_ORG="$1"
OUTDIR="targets-$(date +%Y%m%d%H%M%S)"

mkdir -p "$OUTDIR"

echo "Exporting all data to $OUTDIR in your current directory"

echo "Getting Compute Instance IP addressess..."
gcloud asset search-all-resources \
    --scope=organizations/"$GCP_ORG" \
    --asset-types='compute.googleapis.com/Address' \
    --format="get(additionalAttributes.address)" \
    >> "$OUTDIR"/gcp-compute-ip.txt

echo "Getting Managed DNS names..."
gcloud asset search-all-resources \
    --scope=organizations/"$GCP_ORG" \
    --asset-types='dns.googleapis.com/ManagedZone' \
    --format="get(additionalAttributes.dnsName)" \
    >> "$OUTDIR"/gcp-dns.txt

echo "Getting load balancer public IPs..."
gcloud asset search-all-resources \
    --scope=organizations/"$GCP_ORG" \
    --asset-types='compute.googleapis.com/ForwardingRule' \
    --format="get(additionalAttributes.address)" \
    >> "$OUTDIR"/gcp-lb.txt

echo "Getting Cloud Run URLs"
gcloud asset search-all-resources \
    --scope=organizations/"$GCP_ORG" \
    --asset-types='run.googleapis.com/Service' \
    --format="get(additionalAttributes.statusUrl)" \
    >> "$OUTDIR"/gcp-cloud-run.txt

echo "Getting App Engine host names"
gcloud asset search-all-resources \
    --scope=organizations/"$GCP_ORG" \
    --asset-types='appengine.googleapis.com/Application' \
    --format="get(additionalAttributes.defaultHostname)" \
    >> "$OUTDIR"/gcp-app-engine.txt

echo "Getting Cloud Function trigger URLs"
gcloud asset search-all-resources \
    --scope=organizations/"$GCP_ORG" \
    --asset-types='cloudfunctions.googleapis.com/CloudFunction' \
    --format="get(additionalAttributes.httpsTriggerUrl)" \
    >> "$OUTDIR"/gcp-cloud-function.txt

# Create a single combined target file
# Also, remove trailing .'s at end of DNS entry values (ex: google.com. -> google.com)
sort -u "$OUTDIR"/*.txt | sed 's/\.$//' > "$OUTDIR"/all.txt

echo "All done, here are your results:"
find "$OUTDIR" -type f -exec sh -c 'echo -n "$1: "; wc -l < "$1"' sh {} \;
