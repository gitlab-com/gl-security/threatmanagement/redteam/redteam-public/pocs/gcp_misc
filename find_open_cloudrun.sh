#!/bin/bash

#############################
# Run this tool to find Cloud Run services that permit unauthenticated
# invocations anywhere in your GCP organization.
# Enjoy!
#############################

for proj in $(gcloud projects list --format="get(projectId)"); do
    echo "[*] scraping project $proj"

    enabled=$(gcloud services list --project "$proj" | grep "Cloud Run API")

    if [ -z "$enabled" ]; then
	continue
    fi


    for run in $(gcloud run services list --platform managed --quiet --project "$proj" --format="get(name)"); do
        ACL="$(gcloud run services get-iam-policy "$run" --platform managed --project "$proj")"

        all_users="$(echo "$ACL" | grep allUsers)"
        all_auth="$(echo "$ACL" | grep allAuthenticatedUsers)"

        if [ -z "$all_users" ]
        then
              :
        else
              echo "[!] Open to all users: $proj: $run"
        fi

        if [ -z "$all_auth" ]
        then
              :
        else
              echo "[!] Open to all authenticated users: $proj: $run"
        fi
    done
done
